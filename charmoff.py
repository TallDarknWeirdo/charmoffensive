import argparse
import dpv
import json
import os.path
import random
import re
from rich.text import Text
from rich.console import Console
from rich.markdown import Markdown as RMarkdown
import string
from textual.app import App, ComposeResult
from textual.widgets import DataTable, Static, Markdown
from textual.screen import ModalScreen
from textual.containers import Horizontal, Vertical
from csv import DictReader

# define some styled status texts
STATUS_UNIDENTIFIED = Text("UNIDENTIFIED", style="#FF0000")
STATUS_NOFEEDER = Text("NO FEEDER", style="#FF7700")
STATUS_PLACED = Text("PLACED", style="#00FF00")
STATUS_IGNORED = Text("IGNORED", style="#555555")
STATUS_FIDUCIAL = Text("FIDUCIAL", style="#0000FF")

# Argument setup.
parser = argparse.ArgumentParser(
    description="Charm Offensive creates Charmhigh DPV files from a combination of KiCad position files and JSON descriptions of inventory, feeders, fiducials and ignore patterns."
)
parser.add_argument(
    "filename",
    help="Input KiCad file. (CSV format, Origin at lower left of PCB, Y coordinates increasing upward.)",
)
parser.add_argument(
    "-inv",
    metavar="filename.json",
    default="inventory.json",
    help="JSON file for inventory. inventory.json if not specified.",
)
parser.add_argument(
    "-feed",
    metavar="filename.json",
    default="feeders.json",
    help="JSON file for feeders. feeders.json if not specified.",
)
parser.add_argument(
    "-ig",
    metavar="filename.json",
    default="ignores.json",
    help="JSON file for designators to ignore. ignores.json if not specified.",
)
parser.add_argument(
    "-fid",
    metavar="filename.json",
    default="fiducials.json",
    help="JSON file for designators to treat as fiducials. fiducials.json if not specified.",
)
parser.add_argument(
    "-auto",
    help="Produce output files automatically (non-interactive mode)",
    action="store_true",
)
parser.add_argument(
    "-seg",
    help="Segment output files. Creates a folder of individual files by feeder",
    action="store_true",
)

args = parser.parse_args()

# Initiate a few variables
aliases = {}
ignoredata = []
uniquerefs = []

# Check that all the needed files exist.
if not os.path.exists(args.filename):
    print("The file " + args.filename + " does not exist.")
    quit()
if not os.path.exists(args.inv):
    print("The file " + args.inv + " does not exist.")
    quit()
if not os.path.exists(args.feed):
    print("The file " + args.feed + " does not exist.")
    quit()
if not os.path.exists(args.ig):
    print("The file " + args.ig + " does not exist.")
    quit()
if not os.path.exists(args.fid):
    print("The file " + args.fid + " does not exist.")
    quit()

# Load and parse all data files
brokenfilemessage = """\
---
## This file doesn't look right.
- Use Place > Drill/Place File Origin to place a coordinate origin at the LOWER LEFT CORNER of your PCB.
- In Preferences > PCB Editor > Origins & Axes set Y Axis for "Increases Up"
- Use File > Fabrication Outputs > Component Placement
- Set format to CSV, Units to MM, and make sure Use Drill/Place file origin is CHECKED.
---
"""
brokenfile = RMarkdown(brokenfilemessage)


# return a string with y random characters. This is defined here because it's used in the parser.
def random_char(y):
    return "".join(random.choice(string.ascii_letters) for x in range(y))


with open(args.inv, "r") as f:
    inventorydata = json.load(f)
with open(args.feed, "r") as f:
    feederdata = json.load(f)
with open(args.fid, "r") as f:
    fiducialdata = json.load(f)
with open(args.ig, "r") as f:
    ignoredata = json.load(f)
with open(args.filename, "r") as f:
    bomdata = list(DictReader(f))
try:
    for placement in bomdata:
        if placement["Ref"] not in uniquerefs:  # Unique Ref
            uniquerefs.append(placement["Ref"])
        else:  # Duplicate Ref
            placement.update({"Ref": placement["Ref"] + random_char(4)})
            uniquerefs.append(placement["Ref"])
        # Synthesize designators from available info.
        designator = (
            re.sub(r"[0-9]+", "", placement["Ref"])
            + placement["Val"]
            + "_"
            + placement["Package"]
        )
        designator = designator.upper()
        placement.update({"Designator": designator})
        placement.update({"Status": STATUS_UNIDENTIFIED})
except:  # Exception thrown during file parse, probably something in the file is broken.
    console = Console()
    console.print(brokenfile)
    quit()

# GUI setup for help screen
helpmarkdown = """\
## Keys
- [tab] Moves between panels
- [p] Adds the part selected in the PLACEMENT panel to the INVENTORY.
- [a] Adds an alias for the selected INVENTORY part, using the selected PLACEMENT.
- [+ / -] Change rotation correction for selected INVENTORY part.
- [l] Load the selected INVENTORY part into the selected FEEDER.
- [d] Delete the selected INVENTORY part.
- [i] Ignore all instances of selected PLACEMENT part.
- [f] Mark instances of selected PLACEMENT part as fiducial markers.
- [e] export DPV file.
- [q]uit
## The mouse also works for scrolling and selecting in all panels.
## File Preperation
- Use Place > Drill/Place File Origin to place a coordinate origin at the LOWER LEFT CORNER of your PCB.
- In Preferences > PCB Editor > Origins & Axes set Y Axis for "Increases Up"
- Use File > Fabrication Outputs > Component Placement
- Set format to CSV, Units to MM, and make sure Use Drill/Place file origin is CHECKED.
"""


class HelpBox(ModalScreen):
    def compose(self) -> ComposeResult:
        yield Markdown(helpmarkdown)

    def on_key(self) -> None:
        self.app.pop_screen()


# GUI setup for export message.
exportedmessage = """\
# File exported!
"""


class ExportBox(ModalScreen):
    def compose(self) -> ComposeResult:
        yield Markdown(exportedmessage)

    def on_key(self) -> None:
        self.app.pop_screen()


class TableApp(App):
    CSS_PATH = "styles.tcss"

    def get_selected_cel(self, tableID, index):
        table = self.query(tableID).first()
        rowkey, _ = table.coordinate_to_cell_key(table.cursor_coordinate)
        return table.get_row(rowkey)[index]

    def updatetables(self) -> None:  # Refresh table content
        # ALIAS LOOKUP TABLE
        aliases.clear()
        for item in inventorydata:
            aliases.update({item: item})
            for alias in inventorydata[item]["Aliases"]:
                aliases.update({alias: item})

        # PLACEMENT TABLE
        bomtable = self.query("#placements").first()
        savedposition = bomtable.cursor_row
        bomtable.clear()
        for rowentry in bomdata:
            feeder = 0
            rowentry["Status"] = STATUS_UNIDENTIFIED
            if rowentry["Designator"] in aliases.keys():
                rowentry["Status"] = STATUS_NOFEEDER
                if aliases[rowentry["Designator"]] in feederdata.values():
                    rowentry["Status"] = STATUS_PLACED
                    feeder = list(feederdata.keys())[
                        list(feederdata.values()).index(aliases[rowentry["Designator"]])
                    ]
            if rowentry["Designator"] in ignoredata:
                rowentry["Status"] = STATUS_IGNORED

            if rowentry["Designator"] in fiducialdata:
                rowentry["Status"] = STATUS_FIDUCIAL

            bomtable.add_row(
                rowentry["Status"],
                feeder,
                rowentry["Ref"],
                rowentry["Designator"],
                key=rowentry["Ref"],
            )
        bomtable.sort("Designator")
        bomtable.move_cursor(row=savedposition)

        # INVENTORY TABLE
        inventorytable = self.query("#inventory").first()
        savedposition = inventorytable.cursor_row
        inventorytable.clear()
        for item in aliases.keys():
            des = aliases[item]
            al = item
            if des == al:
                al = ""  # If I'm a part, blank by alias column for readability
            else:
                des = (
                    ""  # If I'm an alias, blank the designator column for readability.
                )
            inventorytable.add_row(
                des, al, inventorydata[aliases[item]]["Rotation"], key=item
            )
        inventorytable.move_cursor(row=savedposition)

        # FEEDER TABLE
        feedertable = self.query("#feeders").first()
        savedposition = feedertable.cursor_row
        feedertable.clear()
        for item in feederdata.keys():
            feedertable.add_row(item, feederdata[item], key=item)
        feedertable.move_cursor(row=savedposition)

    # GUI Setup for main window.
    def compose(self) -> ComposeResult:
        with Horizontal():
            with Vertical():
                yield Static(args.filename)
                yield DataTable(id="placements")

            with Vertical():
                with Vertical():
                    yield Static("Hit <h> for help.  ", id="helpbar")
                    yield Static(args.feed)
                    yield DataTable(id="feeders")
                with Vertical():
                    yield Static("")
                    yield Static(args.inv)
                    yield DataTable(id="inventory")
                    yield Static("")

    def on_mount(self) -> None:
        # Let's set up the table columns and styling
        helpbar=self.query('#helpbar').first()
        helpbar.styles.height=1
        helpbar.styles.text_align = "right"
        helpbar.styles.text_style = "bold"

        bomtable = self.query("#placements").first()
        bomtable.cursor_type = "row"
        bomtable.add_column("Status", key="Status")
        bomtable.add_column("Fdr.", key="Feeder")
        bomtable.add_column("Ref", key="Ref")
        bomtable.add_column("Designator", key="Designator")

        inventorytable = self.query("#inventory").first()
        inventorytable.cursor_type = "row"
        inventorytable.add_columns("Designator")
        inventorytable.add_columns("Alias")
        inventorytable.add_columns("Angle Correction")
        inventorytable.styles.overflow_y = "scroll"


        feedertable = self.query("#feeders").first()
        feedertable.cursor_type = "row"
        feedertable.add_column("Feeder", key="feeder")
        feedertable.add_column("Designator", key="designator")

        self.updatetables()  # populate them with data

        if args.auto:  # If we're on autopilot, we should generate a file and GTFO.
            self.key_e()
            self.key_q()

    def key_p(self) -> None:  # Add Part
        ref = self.get_selected_cel("#placements", 2)
        bom_index = next(  # ugly hack to get key from value
            (index for (index, d) in enumerate(bomdata) if d["Ref"] == ref), None
        )
        inventorydata.update(
            {
                bomdata[bom_index]["Designator"]: {
                    "Rotation": 0,
                    "TapeSize": 0,
                    "Aliases": [],
                }
            }
        )
        self.updatetables()
        with open("inventory.json", "w") as f:  # Persist changes to file.
            json.dump(inventorydata, f, indent=4)

    def key_a(self) -> None:  # Add Alias for part
        newalias = self.get_selected_cel("#placements", 3)
        targetdesignator = self.get_selected_cel("#inventory", 0)
        if (
            targetdesignator == ""
        ):  # User is trying to add alias to alias, fetch parent designator instead.
            alias = self.get_selected_cel("#inventory", 1)
            targetdesignator = aliases[alias]
        inventorydata[targetdesignator]["Aliases"].append(newalias)
        self.updatetables()
        with open("inventory.json", "w") as f:  # Persist changes to file.
            json.dump(inventorydata, f, indent=4)

    def key_i(self) -> None:  # Mark as ignored (toggle)
        ref = self.get_selected_cel("#placements", 2)
        bom_index = next(  # ugly hack to get key from value
            (index for (index, d) in enumerate(bomdata) if d["Ref"] == ref), None
        )
        designator = bomdata[bom_index]["Designator"]
        if designator in ignoredata:
            ignoredata.remove(designator)
        else:
            ignoredata.append(designator)
        with open("ignores.json", "w") as f:  # Update file
            json.dump(ignoredata, f, indent=4)
        self.updatetables()

    def key_f(self) -> None:  # Mark as Fiducial (toggle)
        ref = self.get_selected_cel("#placements", 2)
        bom_index = next(  # ugly hack to get key from value
            (index for (index, d) in enumerate(bomdata) if d["Ref"] == ref), None
        )
        designator = bomdata[bom_index]["Designator"]
        if designator in fiducialdata:
            fiducialdata.remove(designator)
        else:
            fiducialdata.append(designator)
        with open("fiducials.json", "w") as f:  # Save changes to file.
            json.dump(fiducialdata, f, indent=4)
        self.updatetables()

    def key_h(self) -> None:  # Display Help
        self.push_screen(HelpBox())

    def key_l(self) -> None:  # Load onto feeder
        lookup = self.get_selected_cel("#inventory", 0)
        if lookup == "":  # Blank designator suggests it must be an alias
            lookup = self.get_selected_cel("#inventory", 1)
        designator = aliases[
            lookup
        ]  # this gives us a Designator, regardless of whether the input is an Alias or not.
        feedernumber = self.get_selected_cel("#feeders", 0)
        feederdata[feedernumber] = designator
        with open("feeders.json", "w") as f:  # Save changes to file.
            json.dump(feederdata, f, indent=4)
        self.updatetables()

    def key_d(self) -> None:  # Delete inventory item
        lookup = self.get_selected_cel("#inventory", 0)
        if lookup == "[EMPTY]":
            return
        if lookup == "":  # Blank designator suggests it must be an alias
            lookup = self.get_selected_cel("#inventory", 1)
            designator = aliases[
                lookup
            ]  # this gives us a Designator, regardless of whether the input is an Alias or not.
            inventorydata[designator]["Aliases"].remove(lookup)
        else:
            del inventorydata[lookup]
        with open("inventory.json", "w") as f:
            json.dump(inventorydata, f, indent=4)
        self.updatetables()

    def key_e(self) -> None:  # Export DPV file
        outputfilename=args.filename[:args.filename.find('.')]+".dpv"
        dpv.write(outputfilename, feederdata, bomdata, inventorydata, aliases, [])
        if (
            not args.auto
        ):  # Skip the completion notification if we're in Auto - it waits for a keypress.
            self.push_screen(ExportBox())

    def key_plus(self) -> None:  # Increase rotation offset of inventory part.
        interval = 90
        ref = self.get_selected_cel("#inventory", 0)
        alias = self.get_selected_cel("#inventory", 1)
        if alias == "":
            lookup = ref
        else:
            lookup = aliases[alias]
        rotation = inventorydata[lookup]["Rotation"]
        rotation = rotation + interval
        if rotation > 180:  # angles beyond 360 degrees get mod'd out.
            rotation = 0-(rotation % 180)
        if rotation < -180:
            rotation = 0 + (rotation % -180)
        inventorydata[lookup]["Rotation"] = rotation
        with open("inventory.json", "w") as f:  # save changes to file.
            json.dump(inventorydata, f, indent=4)
        self.updatetables()

    def key_minus(self) -> None:  # Decrease rotation offset of inventory part.
        # This code is basically verbatim from the key_plus function and should probably get abstracted into a rotate function rather than repeating.
        interval = -90
        ref = self.get_selected_cel("#inventory", 0)
        alias = self.get_selected_cel("#inventory", 1)
        if alias == "":
            lookup = ref
        else:
            lookup = aliases[alias]
        rotation = inventorydata[lookup]["Rotation"]
        rotation = rotation + interval
        if rotation > 180:  # angles beyond 360 degrees get mod'd out.
            rotation = 0-(rotation % 180)
        if rotation < -180:
            rotation = 0 + (rotation % -180)
        inventorydata[lookup]["Rotation"] = rotation
        with open("inventory.json", "w") as f:  # save changes to file.
            json.dump(inventorydata, f, indent=4)
        self.updatetables()

    def key_q(self) -> None:  # Quit.
        quit()  # Fine. Be that way!


app = TableApp()
if __name__ == "__main__":
    app.run()
