import json
import csv
import datetime
from rich.text import Text

STATUS_PLACED = Text("PLACED", style="#00FF00")
STATUS_FIDUCIAL = Text("FIDUCIAL", style="#0000FF")


def write(filename, feederdata, bomdata, inventorydata, aliases, fiducialdata):
    outputfile = open(filename, "w")

    # ----------- Preamble
    outputfile.write("separated" + "\n")
    outputfile.write("FILE," + filename + "\n")
    outputfile.write("PCBFILE,PCB-CHARMOFFENSIVE" + "\n")
    outputfile.write("DATE," + datetime.datetime.now().strftime("%Y/%m/%d") + "\n")
    outputfile.write("TIME," + datetime.datetime.now().strftime("%H:%M:%S") + "\n")
    outputfile.write("PANELTYPE,0" + "\n")
    outputfile.writelines("\n")

    # ----------- TABLE 1 - Feeders
    fieldnames = [
        "Table",
        "No.",
        "ID",
        "DeltX",
        "DeltY",
        "FeedRates",
        "Note",
        "Height",
        "Speed",
        "Status",
        "SizeX",
        "SizeY",
    ]

    writer = csv.DictWriter(outputfile, fieldnames=fieldnames)

    index = 0
    outputfile.writelines("\n")
    writer.writeheader()

    for rowentry in list(feederdata.keys()):
        if feederdata[rowentry] != "[EMPTY]":
            writer.writerow(
                {
                    "Table": "Station",
                    "No.": index,
                    "ID": rowentry,
                    "DeltX": 0,
                    "DeltY": 0,
                    "FeedRates": 4,
                    "Note": feederdata[rowentry],
                    "Height": 0.5,
                    "Speed": 0,
                    "Status": 6,  # Who knows why?
                    "SizeX": 0,
                    "SizeY": 0,
                }
            )
            index = index + 1

    # ----------- TABLE 2 - Panel Repeats
    writer = csv.DictWriter(
        outputfile, fieldnames=["Table", "No.", "ID", "DeltX", "DeltY"]
    )
    outputfile.writelines("\n")
    writer.writeheader()
    writer.writerow({"Table": "Panel_Coord", "No.": 0, "ID": 1, "DeltX": 0, "DeltY": 0})

    # ----------- TABLE 3 - Placements
    fieldnames = [
        "Table",
        "No.",
        "ID",
        "PHead",
        "STNo.",
        "DeltX",
        "DeltY",
        "Angle",
        "Height",
        "Skip",
        "Speed",
        "Explain",
        "Note",
    ]

    writer = csv.DictWriter(outputfile, fieldnames=fieldnames)

    index = 0
    outputfile.writelines("\n")
    writer.writeheader()
    for rowentry in sorted(bomdata, key=lambda x: x["Designator"]):
        feeder = 0

        if rowentry["Status"] == STATUS_PLACED:
            feeder = list(feederdata.keys())[
                list(feederdata.values()).index(aliases[rowentry["Designator"]])
            ]

            rotation = float(rowentry["Rot"])

            offset = float(inventorydata[aliases[rowentry["Designator"]]]["Rotation"])
            rotation = rotation + offset
            if rotation > 180:
                rotation = 0 - (rotation % 180)
            if rotation < -180:
                rotation = 0 + (rotation % 180)

            writer.writerow(
                {
                    "Table": "EComponent",
                    "No.": index,
                    "ID": index + 1,
                    "PHead": 1,  # Pickup Nozzle
                    "STNo.": feeder,
                    "DeltX": rowentry["PosX"],
                    "DeltY": rowentry["PosY"],
                    "Angle": rotation,
                    "Height": 0.5,
                    "Skip": 6,  # Who knows why?
                    "Speed": 0,
                    "Explain": rowentry["Ref"],
                    "Note": rowentry["Designator"],
                }
            )
            index = index + 1

    # ----------- TABLE 4 - This is for auxiliary tray pickups. Should I implement it with another JSON?
    fieldnames = [
        "Table",
        "No.",
        "ID",
        "CenterX",
        "CenterY",
        "IntervalX",
        "IntervalY",
        "NumX",
        "NumY",
        "Start",
    ]
    writer = csv.DictWriter(outputfile, fieldnames=fieldnames)
    outputfile.writelines("\n")
    writer.writeheader()

    # ----------- TABLE 5 - This seems to be for "internal" use keeping track of calibrations?
    fieldnames = ["Table", "No.", "nType", "nAlg", "nFinished"]
    writer = csv.DictWriter(outputfile, fieldnames=fieldnames)
    outputfile.writelines("\n")
    writer.writeheader()
    writer.writerow(
        {"Table": "PcbCalib", "No.": 0, "nType": 0, "nAlg": 0, "nFinished": 0}
    )

    # ----------- TABLE 6 - Calibration marks and fiducials
    fieldnames = ["Table", "No.", "ID", "offsetX", "offsetY", "Note"]
    writer = csv.DictWriter(outputfile, fieldnames=fieldnames)
    outputfile.writelines("\n")
    writer.writeheader()
    index = 0
    for rowentry in bomdata:
        if rowentry["Status"] == STATUS_FIDUCIAL:
            writer.writerow(
                {
                    "Table": "CalibPoint",
                    "No.": index,
                    "ID": index + 1,
                    "offsetX": rowentry["PosX"],
                    "offsetY": rowentry["PosY"],
                    "Note": rowentry["Ref"],
                }
            )
            index = index + 1

    outputfile.close()
