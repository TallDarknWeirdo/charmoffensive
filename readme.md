![screenshot](screenshot.png)

## What is Charm Offensive?
Charm Offensive takes the placement files produced by KiCad, combines them with data about parts, feeders, fiducials and no-parts (ignores) to produce the DPV files used by the execrable Charmhigh software. 

The interface is text-mode, but mouse enabled (indeed, mousing for selections/scrolling and keyboard for actions is how it was designed to be used, you CAN operate exclusively with keyboard, but it's slower.) Speaking of the interface, 80x24 ain't gonna cut it most of the time, the interface is responsive, scale it how you want it.

## Limitations?
Part pitch is not implemented yet, it sets them all to 4mm. That's coming soon.

## How is this different from the scripts published by SparkFun?
While all the secondary data for Charm Offensive is stored in external JSON flat files, all the essential functions (identifying parts, defining aliases, setting per-part rotation corrections, defining fiducials, and configuring feeders) can be performed interactively inside Charm Offensive, with the results immediately visible. The goal is to be able to create a ready-to-run file without having to edit any files externaly. (beyond setting a blank feeder file for your machine, more on that below)

## How do I install and run it?
Charm Offensive is just a small pile of Python, so it doesn't need an installation per se, but it does have dependencies listed in requirements.txt. You can install all the dependencies with pip ---

```bash
pip3 install -r requirements.txt
```

This is the sort of thing you might want to consider using a VENV for, but I'm not your mom.

The most basic usage would look like:
```bash
python3 charmoff.py placementfile
```

## How should I prepare my KiCad files?
- Use Place > Drill/Place File Origin to place a coordinate origin at the LOWER LEFT CORNER of your PCB.
- In Preferences > PCB Editor > Origins & Axes set Y Axis to "Increases Up"
- Use File > Fabrication Outputs > Component Placement
- Set format to CSV, Units to MM, and make sure 'Use Drill/Place file origin' is CHECKED.

## How do I set up the feeders to match my machine?
The feeders.json file that comes with Charm Offensive is set up to resemble a CHMT36VA, if your feeder configuration/quantity is different, just edit feeders.json to taste - the format is self explanatory.

### I want to automate this part of production, is there a non-interactive version?
There is! Charm Offensive is perfectly happy to take input files and immediately output a DPV based on their contents without user interaction. Use the help flag to learn all the appropriate flags and syntax:

```bash
python3 charmoff -h
```

### What is the state of development?
I would call this a beta. It seems to work, but keep an eye on the output and use at your own risk.Since I use it myself, it'll likely stay in slow but active development at least until I run out of features I want and bugs I can reproduce. 

### Will this work on my operating system?
Almost certainly. The interface stuff is handled by the excellent Textual framework, which seems to cope with varied terminal environments just fine. I test it against Linux Mint and MacOS. 

### Are there known bugs?
A few.

- The scroll behavior is irritating on long placement files, I'm working on that.
- KiCad (for some reason) allows multiple parts to have identical References, which the internal architecture of Charm Offensive does not. If it encounters multiple identical references in a file, it will append four random characters to each. Not exactly a bug, but an ugly hack.
- Very few errors are captured by exceptions, so if it gets mad, it tends to just crash. I hope you'll submit these and the files used to produce them as Issues, so I can track them down.
- Probably there are many more bugs which I have not yet discovered. I'm not a full time software developer, so the codebase isn't always beautiful.